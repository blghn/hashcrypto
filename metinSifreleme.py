#!/usr/bin/python3
# -*- coding: utf-8 -*-
__author__ = 'blghn - fnym'


from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import hashlib  # hashlib kütüphanesi verilerimizi farklı algoritmalarda şifrelememizi sağlayan bir kütüphanedir
import sys

sifreTarz = """

QLineEdit
{
border:1px solid #b1b1b1;
border-radius:6;
}
QPushButton
{
    border-width: 1px;
    border-color: #1e1e1e;
    border-style: solid;
    border-radius: 6;
    padding: 3px;
    font-size: 12px;
    padding-left: 5px;
    padding-right: 5px;
}


QComboBox
{
    border-style: solid;
    border: 1px solid #1e1e1e;
    border-radius: 5;
}
QComboBox:hover,QPushButton:hover
{
    border: 2px solid QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);
}


QComboBox:on
{
    padding-top: 3px;
    padding-left: 4px;
    selection-background-color: #ffaa00;
}

QComboBox QAbstractItemView
{
    border: 2px solid darkgray;
    selection-background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);
}

QComboBox::drop-down
{
     subcontrol-origin: padding;
     subcontrol-position: top right;
     width: 15px;

     border-left-width: 0px;
     border-left-color: darkgray;
     border-left-style: solid; /* just a single line */
     border-top-right-radius: 3px; /* same radius as the QComboBox */
     border-bottom-right-radius: 3px;
 }

QComboBox::down-arrow
{
     image: url(noimg);
}
QStatusBar
{
    padding-left:8px;
    background:#b1b1b1;
    color:black;
    font-weight:bold;
}
"""


class sifreOlusturucuGui(QWidget):
    def __init__(self, ebeveyn=None):
        super().__init__(ebeveyn)
        self.ebeveyn = ebeveyn
        self.setStyleSheet(sifreTarz)
        self.setMinimumWidth(300)
        self.setMaximumWidth(500)
        self.setMaximumHeight(115)

        self.setWindowTitle("Şifre Oluşturucu")

        grid = QGridLayout()
        butonLayout = QHBoxLayout()

        self.sifreGosterim = QLineEdit()
        self.sifreGosterim.setReadOnly(True)
        grid.addWidget(self.sifreGosterim, 0, 0, 1, 2)

        grid.addWidget(QLabel("Şifrelencek Metin: "), 1, 0)
        self.metinGiris = QLineEdit()
        grid.addWidget(self.metinGiris, 1, 1)

        grid.addWidget(QLabel("Şifreleme Formatı: "), 2, 0)
        self.formatSecim = QComboBox()
        self.formatSecim.addItems(
            ["md5", "sha1", "sha224", "sha256", "sha384", "sha512"])
        grid.addWidget(self.formatSecim, 2, 1)

        self.sifreleButon = QPushButton("Şifrele", self)
        butonLayout.addWidget(self.sifreleButon)
        self.sifirlaButon = QPushButton("Sıfırla", self)
        butonLayout.addWidget(self.sifirlaButon)
        grid.addLayout(butonLayout, 3, 0, 1, 2)

        self.sifreleButon.clicked.connect(self.sifrele)
        self.sifirlaButon.clicked.connect(self.sifirla)

        self.setLayout(grid)

    def sifrele(self):
        metin = self.metinGiris.text()
        if metin == "" or metin == None:
            # dialog kutusunu öldürmeye yarar
            self.setAttribute(Qt.WA_DeleteOnClose)
            self.sifreGosterim.clear()
            return
        sifrelemeFormati = self.formatSecim.currentText()

        if sifrelemeFormati == "md5":
            sifreliMetin = self.sifreleMD5(metin)
        if sifrelemeFormati == "sha1":
            sifreliMetin = self.sifreleSHA1(metin)
        if sifrelemeFormati == "sha224":
            sifreliMetin = self.sifreleSHA224(metin)
        if sifrelemeFormati == "sha256":
            sifreliMetin = self.sifreleSHA256(metin)
        if sifrelemeFormati == "sha384":
            sifreliMetin = self.sifreleSHA384(metin)
        if sifrelemeFormati == "sha512":
            sifreliMetin = self.sifreleSHA512(metin)

        self.sifreGosterim.clear()
        self.sifreGosterim.setText(str(sifreliMetin))
        self.ebeveyn.status.showMessage(
            "{} formatında şifre oluşturuldu".format(sifrelemeFormati), 20000)

    def sifirla(self):
        self.sifreGosterim.clear()
        self.formatSecim.setCurrentIndex(0)
        self.metinGiris.clear()
        self.ebeveyn.status.showMessage(" ")

    def sifreleMD5(self, sifrelenecekMetin):
        return str(hashlib.md5(sifrelenecekMetin.encode("utf-8")).hexdigest())

    def sifreleSHA1(self, sifrelenecekMetin):
        return str(hashlib.sha1(sifrelenecekMetin.encode("utf-8")).hexdigest())

    def sifreleSHA224(self, sifrelenecekMetin):
        return str(hashlib.sha224(sifrelenecekMetin.encode("utf-8")).hexdigest())

    def sifreleSHA256(self, sifrelenecekMetin):
        return str(hashlib.sha256(sifrelenecekMetin.encode("utf-8")).hexdigest())

    def sifreleSHA384(self, sifrelenecekMetin):
        return str(hashlib.sha384(sifrelenecekMetin.encode("utf-8")).hexdigest())

    def sifreleSHA512(self, sifrelenecekMetin):
        return str(hashlib.sha512(sifrelenecekMetin.encode("utf-8")).hexdigest())


class anaPencere(QMainWindow):
    def __init__(self, ebeveyn=None):
        super().__init__(ebeveyn)
        self.sifreOlusturucuWidget = sifreOlusturucuGui(self)
        self.setCentralWidget(self.sifreOlusturucuWidget)
        self.setStyleSheet(sifreTarz)
        self.setMinimumWidth(300)
        self.setMaximumWidth(500)
        self.setMaximumHeight(115)
        self.center()  # Kendi yaptığımız ortalama yapan fonksiyonumuz.

        self.setWindowTitle("Şifre Oluşturucu")
        self.setWindowIcon(QIcon(":icon.png"))

        self.status = self.statusBar()

    def center(self):  # Pencereyi orta alanda çıkardık.
        # geometry of the main window
        qr = self.frameGeometry()
        # center point of screen
        cp = QDesktopWidget().availableGeometry().center()
        # move rectangle's center point to screen's center point
        qr.moveCenter(cp)
        # top left of rectangle becomes top left of window centering it
        self.move(qr.topLeft())


uyg = QApplication(sys.argv)
pen = anaPencere()
pen.show()
# pen = sifreOlusturucuGui()
# pen.show()
uyg.exec_()
